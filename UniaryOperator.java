
public class UniaryOperator {
	public static void main(String[] args) {
		
		int a = 20;
		System.out.println(a);
		System.out.println(a++);
		System.out.println(++a);
		int c = a;
		System.out.println(c);
		System.out.println(c++);
		System.out.println(--c);
		System.out.println(++c);
		System.out.println(c++);
		System.out.println(c--);
		
		float z=c;
		System.out.println(z);
		System.out.println(z+1);
		System.out.println(z++);
		System.out.println(--z);
		
		//Short notation
		int w= (int) (z+z);
		System.out.println(w);
		System.out.println(z+=z);
		z-=z;
		z+=1;
		System.out.println(z);
		
}
}
