
public class StringEx {
	// 1. String literal
	// 2. String Object
	public static void main(String[] args) {
	String str1 = "Operator"; //String literal
	char chr[] = {'O','p','e','r','a','t','o','r'};
	
	String str2 = new String ("Operator");
	String str5 = new String ("Operator"); // String with new keyword
	
	System.out.println(str1);
	System.out.println(str2);
	
	}

}
