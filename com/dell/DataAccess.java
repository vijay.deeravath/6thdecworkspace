package com.dell;

public class DataAccess {
	
	//private is accessible within a class with in package.
	private static int privateV;
	// default is accessible within a package. 
	static int defaultV;
	// protected is accessible within a class, package ,outside package but with inheritance
	protected static int protectedV;
	// public is accessible anywhere.
	public int publicV;
	
	//inner class
	private class PrivateClass{}
	class DefaultClass{}
	protected class ProtectedClass{}
	public class PublicClass{}
	
	private void privateMethod() {}
	void defaultMethod () {}
	protected int protectedMethod() {return 10;} 
	public void publicMethod () {}
	
		
	public static void main(String[] args) {
		System.out.println("data"+ privateV);
		System.out.println("data"+ privateV);
	}

}
