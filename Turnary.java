
public class Turnary {
	public static void main(String[] args) {
		int age=21;
		int z=0;
		if(age>20){
			z=age;
		} else { 
			z=0;
		}
		
		//turnary operator
		z=(age>20) ? age: 0;
		System.out.println(z);
		
		String loginMsg = (z!=0) ? "Login Success" : "Login Failed";
		System.out.println(loginMsg);
		
		z=(age>20) ? age: (age>18)? 18 : 0;
		System.out.println(z);
		String loginMsg1 = (z!=0) ? "Login Success" : "Login Failed";
		System.out.println(loginMsg);
		
		
		
		
		
		
		
		
	}

}
